﻿using System;
using System.Reflection;
using System.Threading;

namespace MultiThread
{
    public static class Program
    {
        static void Main(string[] args)
        {
            new Main().Run();
        }
    }

    class Main
    {
        List<int> leaderboard = new List<int>();
        private object createlockObj = new object();
        private object lockObj = new object();

        public void Run()
        {
            int HorseCount = 10;

            new Thread(() => Update(HorseCount)).Start();
            new Thread(() => Horse(1, HorseCount)).Start();
        }

        private void Update(int HorseCount)
        {
            while (leaderboard.Count < HorseCount)
            {
                Thread.Sleep(1);
            }
            lock (lockObj)
            {
                Console.WriteLine();
                for (int i = 0; i < HorseCount; i++)
                {
                    Console.WriteLine("Horse " + leaderboard[i].ToString() + " is " + getRank(i));
                }
            }
        }

        private void Horse(int index, int HorseCount)
        {
            if (index < HorseCount)
            {
                new Thread(() => Horse(index + 1, HorseCount)).Start();
            }

            int i = 0;
            while (i < 100)
            {
                // 1. 숫자를 증가시킨다.
                i++;

                // 2. 증가시킨 숫자를 출력한다.
                Console.WriteLine("Horse " + index.ToString() + " - " + i.ToString());

                // 3. 랜덤한 숫자를 50에서 100 사이에서 랜덤한 숫자 하나를 가져온다.
                int sleepTime = new Random().Next(50, 101);

                // 4. 랜덤한 매우 짧은 시간(50~100ms)동안 쓰레드를 중지시킨다.
                Thread.Sleep(sleepTime);
            }
            lock (lockObj)
            {
                leaderboard.Add(index);
            }
        }

        private string getRank(int i)
        {
            if (i == 0)
                return "1st";
            else if (i == 1)
                return "2nd";
            else if (i == 2)
                return "3rd";
            else
                return (i + 1).ToString() + "th";
        }
    }
}