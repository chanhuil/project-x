﻿using System;
using System.Reflection;
using System.Threading;

namespace MultiThread
{
    public static class Program
    {
        static int count = 0;
        static object obj = new object();
     
        static void Main(string[] args)
        {
            new Thread(() => print(1)).Start();
            new Thread(() => print(2)).Start();
            new Thread(() => print(3)).Start();
        }
        public static void print(int id)
        {
            while (true)
            {
                lock (obj)
                {
                    if (count >= 1000)
                    {
                        return;
                    }
                    count++;
                    Console.WriteLine("{0}번 쓰레드 : {1}", id, count);
                }
            }
        }
    }
}