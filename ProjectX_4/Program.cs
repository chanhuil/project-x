﻿using System;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using System.Timers;

namespace MultiThread
{
    public static class Program
    {
        static bool ended = false;
        static Stopwatch timer = new Stopwatch();

        static void Main(string[] args)
        {
            timer.Start();
            new Thread(() => countFunc()).Start();
            new Thread(() => timeFunc()).Start();
        }
        public static void countFunc()
        {
            long count = 0;
            while (count < 10000000000)
            {
                count++;
            }
            ended = true;
            timer.Stop();

            Console.WriteLine("걸린 시간 : {0}.{1}", timer.ElapsedMilliseconds / 1000, timer.ElapsedMilliseconds % 1000);
        }
        public static void timeFunc()
        {
            int sec = 1000;
            while (!ended)
            {
                if (timer.ElapsedMilliseconds >= sec)
                {
                    Console.WriteLine("증가 중 : {0}초", sec/1000);
                    sec += 1000;
                }
            }
        }
    }
}