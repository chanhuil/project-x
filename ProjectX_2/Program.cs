﻿using System;
using System.Reflection;
using System.Threading;

namespace MultiThread
{
    public static class Program
    {
        static void Main(string[] args)
        {
            new Thread(() => print(1)).Start();
            new Thread(() => print(2)).Start();
            new Thread(() => print(3)).Start();
        }
        
        public static void print(int id)
        {
            for (int i = 1; i <= 100; i++)
            {
                Console.WriteLine("{0}번 쓰레드 : {1}", id, i);
                Thread.Sleep(1);
            }
        }
    }
}