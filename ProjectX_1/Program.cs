﻿using System;
using System.Reflection;
using System.Threading;

namespace MultiThread
{
    public static class Program
    {
        static void Main(string[] args)
        {
            new Thread(() => print()).Start();
            print();
        }

        public static void print()
        {
            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine(i);
                Thread.Sleep(1);
            }
        }
    }
}